<?php

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function logintoboggan_role_form_logintoboggan_main_settings_alter(&$form, &$form_state, $form_id) {

    // This is to specify the vertical tab holder.
    // One page can have two vertical tabs, but it's always one main tab with sub tabs.
    $form['logintoboggan_vertical_tab'] = array(
        '#type' => 'vertical_tabs',
    );
    $form['login']['#group'] = 'logintoboggan_vertical_tab';
    $form['registration']['#group'] = 'logintoboggan_vertical_tab';
    $form['registration']['redirect']['#group'] = 'logintoboggan_vertical_tab';
    $form['other']['#group'] = 'logintoboggan_vertical_tab';
    $form['registration']['logintoboggan_confirm_email_at_registration']['#weight'] = 0;
    $form['registration']['logintoboggan_user_email_verification']['#weight'] = 10;
    $form['registration']['logintoboggan_pre_auth_role']['#weight'] = 20;
    $form['registration']['logintoboggan_purge_unvalidated_user_interval']['#weight'] = 30;
    $form['registration']['logintoboggan_immediate_login_on_register']['#weight'] = 40;
    $form['registration']['redirect']['#weight'] = 50;


    // Grab the roles that can be used for pre-auth. Remove the anon role, as it's not a valid choice.
    $roles = user_roles(TRUE);
    $form ['registration']['logintoboggan_post_auth_role'] = array(
        '#type' => 'select',
        '#title' => t('Post authenticated role'),
        '#options' => $roles,
        '#default_value' => variable_get('logintoboggan_post_auth_role', DRUPAL_AUTHENTICATED_RID),
        '#description' => t('Choose the post authentication role after successfull email validation with logintoboggan.'),
        '#weight' => 21
    );
}

/**
 * Implement hook_user
 */
function logintoboggan_role_user_update(&$edit, $account, $category){
    // This is only fired when a user confirms their email address, logintoboggan style
    if (isset($account->logintoboggan_email_validated) && $account->logintoboggan_email_validated == TRUE) {
        $post_auth_role_rid = variable_get('logintoboggan_post_auth_role');
        if(!empty($post_auth_role_rid)){
            $role = user_role_load($post_auth_role_rid);
            $roles = $account->roles + array($role->rid => $role->name);

            // we have to do this to stop an infinite loop, and also to allow lower weighted modules to possibly do something here
            $user = $account;
            unset($user->logintoboggan_email_validated);

            user_save($user, array('roles' => $roles));
        }
    }
}

/**
 * Returns a role ID based on role name
 *
 * @param $name
 *  name of role to return
 * @return
 *  (int) Role ID
 */
function logintoboggan_role_get_role_by_name($name) {
    return array_search($name, user_roles());
}
